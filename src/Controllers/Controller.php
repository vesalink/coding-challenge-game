<?php
namespace Ucc\Controllers;

use Ucc\Http\JsonResponseTrait;
use Ucc\Services\QuestionService;

class Controller
{
    use JsonResponseTrait; // trait implementation
    protected $requestBody;

    public function __construct()
    {
        $data = file_get_contents('php://input');
        $this->requestBody = json_decode($data);
    }
}