<?php
namespace Ucc\Services;

use JsonMapper;
use KHerGe\JSON\JSON;
use Ucc\Models\Question;

class QuestionService
{
    const QUESTIONS_PATH = __DIR__ . '/../../questions.json';

    private JSON $json;
    private JsonMapper $jsonMapper;

    public function __construct(JSON $json, JsonMapper $jsonMapper)
    {
        $this->json = $json;
        $this->jsonMapper = $jsonMapper;

        $questions = $this->json->decode(file_get_contents(self::QUESTIONS_PATH));
        foreach ($questions as $question) {
            $this->questions[] = $this->jsonMapper->map($question, new Question());
        }
    }

    public function getRandomQuestions(int $count = 5): array
    {
        $random_questions = array();
        for ($i = 0; $i <  $count; $i++) {
            $random_questions[] = $this->questions[rand(0, count($this->questions) - 1)]->jsonSerialize();
        }
        return $random_questions;

    }

    public function getPointsForAnswer(int $id, string $answer): int
    {
        //TODO: Calculate points for the answer given
    }
}